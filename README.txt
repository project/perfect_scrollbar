
General Information
-------------------
The Perfect Scrollbar module integrates the perfect scrollbar jQuery plugin with
Drupal. The perfect Scrollbar module allows the user to add scrollbars to
sections of content within a Drupal website.

You can find out about this jQuery plugin on the developer's website:
https://noraesae.github.io/perfect-scrollbar

The Perfect Scroller jQuery Plugin is developed, maintained, and licensed
independently of this Drupal module.

Downloads
---------
The Perfect Scroller JS plugin is provided together with the module files.


Installation and Usage
----------------------
Install the Perfect Scrollbar module in the usual manner

Drupal Module Author
--------------------
Swapnil P Sarvankar (SwapS)
https://www.drupal.org/user/1488880
